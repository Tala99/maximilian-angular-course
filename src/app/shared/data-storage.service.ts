import { RecipeService } from './../recipes/recipe.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Recipe } from '../recipes/recipe.model';
import { map, tap } from 'rxjs';

@Injectable({providedIn: 'root'}) //we can say this instead of adding this service inside the app.module in the providers array
export class DataStorageService {
    constructor(private http: HttpClient, private recipeService: RecipeService) {}

    storeRecipes() {
        const recipes = this.recipeService.getRecipes();
        //I will be using Put just in case I need to overwrite what's already in there (as if i have previous recipes stored) so i want to it to overwrite and add what's new
        this.http.put('https://my-angular-app-124a3-default-rtdb.asia-southeast1.firebasedatabase.app/recipes.json',recipes).subscribe(
            response => {
                console.log(response);
            }
        ); //we can subscribe here and we can do it in an alternative way which is return this observable and do the subscription in the component  
    }

    fetchRecipes() {
        // we will use pipe(map(...)) in order to make sure that the recipes we want to fetch HAVE ingredients so it won't give me undefined
        //pay attention that the first "map" we had is an rxjs method that works as a transformer but the second "map" works as an array function the normal one(map)
        return this.http.get<Recipe[]>('https://my-angular-app-124a3-default-rtdb.asia-southeast1.firebasedatabase.app/recipes.json')
        .pipe(map(recipes => {
            return recipes.map(recipe => {
                return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
            });
        }),
        tap( recipes => {
            this.recipeService.setRecipes(recipes)
        }))
        // .subscribe(
        //     recipes => {
        //         this.recipeService.setRecipes(recipes);
        //     }
        // )
    }
}