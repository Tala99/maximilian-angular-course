import { EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Ingredient } from './../shared/ingredient.model';
export class ShoppingListService{

    startedEditing = new Subject<number>();
    //ingredientChanged = new EventEmitter<Ingredient[]>();
    ingredientChanged = new Subject<Ingredient[]>();
    
    private ingredients : Ingredient[] = [
        new Ingredient("Apples",5),
        new Ingredient("Tomatoes",10)
      ];
    
    getIngredient(index: number) {
        return this.ingredients[index];
    }
    getIngredients() {
        return this.ingredients.slice(); //we use the slice in order to send a copy of the object as we cannot access it 
    }

    addIngredient(ingredient : Ingredient) {
        this.ingredients.push(ingredient);
        //this.ingredientChanged.emit(this.ingredients.slice());
        this.ingredientChanged.next(this.ingredients.slice());
    }

     addIngredients(ingredients : Ingredient[]) {

        this.ingredients.push(...ingredients); //we added the ... because without them, the function will add the ingredients as if they are one object (because it recieved one array) 
        this.ingredientChanged.next(this.ingredients.slice());
        //this.ingredientChanged.emit(this.ingredients.slice());
    //     for(let ingredient of ingredients) {
    //         this.addIngredient(ingredient);
    //     }
    }

    updateIngredient(index: number, newIngredient: Ingredient) {
        this.ingredients[index] = newIngredient;
        this.ingredientChanged.next(this.ingredients.slice());
    }

    deleteIngredient(index: number) {
        this.ingredients.splice(index, 1);
        this.ingredientChanged.next(this.ingredients.slice());
    }


}