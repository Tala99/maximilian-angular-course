import { Subscription } from 'rxjs';
import { ShoppingListService } from './../shopping-list.service';
import { Ingredient } from './../../shared/ingredient.model';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  //now i want to access the form so that I can let the fields be filled with the element I am clicking on now from the shopping-list IF I am in the edit mode
  @ViewChild('f',{static : false}) slForm: NgForm; 
  // @ViewChild('nameInput', {static:false}) nameInputRef: ElementRef;
  // @ViewChild('amountInput', {static:false}) amountInputRef: ElementRef;
  //@Output() ingredientAdded= new EventEmitter<Ingredient>();
  editMode = false;
  editedItemIndex: number;
  editedItem: Ingredient;
  constructor(private slService : ShoppingListService) { }

  ngOnInit(): void {
    this.subscription = this.slService.startedEditing.subscribe(
      (index: number) => {
        this.editedItemIndex = index;
        this.editMode = true;
        this.editedItem = this.slService.getIngredient(index);
        this.slForm.setValue({
          name: this.editedItem.name,
          amount: this.editedItem.amount
        })
      }
    );
  }

  onAddItem(form: NgForm) {
    const value = form.value;
    // const ingName = this.nameInputRef.nativeElement.value;
    // const amountName = this.amountInputRef.nativeElement.value;
    // const newIngredient = new Ingredient(ingName, amountName);
    const newIngredient = new Ingredient(value.name, value.amount);
    //this.ingredientAdded.emit(newIngredient);
    if(this.editMode) { 
      this.slService.updateIngredient(this.editedItemIndex, newIngredient);
    }
    else{
    this.slService.addIngredient(newIngredient);
  }
  this.editMode = false;
  form.reset();
}

onClear() {
  this.slForm.reset();
  this.editMode = false;
}

onDelete() {
  this.slService.deleteIngredient(this.editedItemIndex);
  this.onClear();
}
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
