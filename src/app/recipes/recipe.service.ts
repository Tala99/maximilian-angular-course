import { ShoppingListService } from './../shopping-list/shopping-list.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Ingredient } from '../shared/ingredient.model';
import { Recipe } from './recipe.model'

@Injectable()
export class RecipeService {

//recipeSelected = new EventEmitter<Recipe>();
recipeSelected = new Subject<Recipe>();
recipesChanged = new Subject<Recipe[]>();

private recipes: Recipe[] = [
    new Recipe('A test recipe', 'This is a desc test', 'https://jshleap.github.io/assets/images/recipes.jpg',[new Ingredient('Meat',2), new Ingredient('French Fries',3)]),
    new Recipe('A test recipe2', 'This is a desc test2', 'https://jshleap.github.io/assets/images/recipes.jpg',[new Ingredient('Chicken',4), new Ingredient('Bread',6)])
      ];

constructor(private slService : ShoppingListService) {}

setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
}

getRecipes() {
    return this.recipes.slice();
}

getRecipe(index: number) {
    return this.recipes[index];
}

addIngredientsToShoppingList(ingredients : Ingredient[]) {
    this.slService.addIngredients(ingredients);
}

addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
}

updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe; 
    this.recipesChanged.next(this.recipes.slice());
}

deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    //now after deleting an item from the recipes, let us inform the recipeschanged and send it a copy of the recipes to see how it looks like now:
    this.recipesChanged.next(this.recipes.slice());
}

}