import { Recipe } from './../recipe.model';
import { RecipeService } from './../recipe.service';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id : number;
  //in order to know if i am in "edit" mode or "adding new" mode: I will store the status :
  editMode = false;
  recipeForm: FormGroup;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService, private router: Router) { }

  ngOnInit(): void {
    //this is to retrieve the id of the thing we are working on on the moment.
    //we added the + to convert it into a number, and we named it 'id' because this is how we named it in the route setup when we sain path:':id' 
    // if the path has Defined "id" it means the thing we want to work on exists which means we will edit it and not add it, therefore if it's not undefined (=defined) the false will become true and the editMode will become true to know that we are in the editMode.
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id']; 
        this.editMode = params['id'] != null; 
        this.initForm(); //this means that whenever the route changes, we need to call this function
        //console.log(this.editMode);
      }
    )
  }

  private initForm() {
    let recipeName = '';
    let recipeImagePath = '';
    let recipeDescription = '';
    let recipeIngredients = new FormArray([]);

    if(this.editMode) {
      const recipe = this.recipeService.getRecipe(this.id);
      recipeName = recipe.name;
      recipeImagePath = recipe.imagePath;
      recipeDescription = recipe.description;
      if(recipe['ingredients']) {
        for (let ingredient of recipe.ingredients) {
          recipeIngredients.push(
            new FormGroup({
              'name': new FormControl(ingredient.name, Validators.required),
              'amount': new FormControl(ingredient.amount, [Validators.required,Validators.pattern(/^[1-9]+[0-9]*$/)])
            })
          )
        }
      }
    }

    this.recipeForm = new FormGroup({
      'name': new FormControl(recipeName, Validators.required),
      'imagePath': new FormControl(recipeImagePath, Validators.required),
      'description': new FormControl(recipeDescription, Validators.required),
      'ingredients': recipeIngredients
    })

    console.log(this.recipeForm.get('ingredients'));

  }

  public get ingertientsFormArray(): FormArray {
    return this.recipeForm.get('ingredients') as FormArray;
  }
  
  onSubmit() {
    //const newRecipe = new Recipe(this.recipeForm.value['name'],  this.recipeForm.value['description'], this.recipeForm.value['imagePath'], this.recipeForm.value['ingredients']);
    if(this.editMode) {
      //this.recipeService.updateRecipe(this.id, newRecipe);
      this.recipeService.updateRecipe(this.id, this.recipeForm.value);

    }
    else {
      // this.recipeService.addRecipe(newRecipe);
      this.recipeService.addRecipe(this.recipeForm.value);
    }
    this.onCancel();
  }

  onAddIngredient() {
    (<FormArray>this.recipeForm.get('ingredients')).push(
      new FormGroup({
        'name': new FormControl(null, Validators.required),
        'amount': new FormControl(null, [Validators.required,Validators.pattern(/^[1-9]+[0-9]*$/)])
      })
    ); //because i want to push 2 inputs, i need to wrap them in form group and not form control
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo:this.route});
  }

  onDeleteIngredient(index: number) {
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }


}
