import { DataStorageService } from './../shared/data-storage.service';
import { Component, Output, EventEmitter} from "@angular/core";

@Component({
    selector:'app-header',
    templateUrl:'./header.component.html'
})
export class HeaderComponent{

    // @Output() feature = new EventEmitter<string>();

    // onSelect(featureInput: string){
    //     this.feature.emit(featureInput);
    // }
    constructor( private dataStorageService: DataStorageService) {}

    onSaveData() {
        this.dataStorageService.storeRecipes();
    }

    onFetchData() {
        this.dataStorageService.fetchRecipes().subscribe();
    }

}